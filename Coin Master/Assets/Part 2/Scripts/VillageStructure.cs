﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillageStructure : MonoBehaviour
{
    public List<GameObject> structureLevels;
    public GameObject buildButtonEnabled;
    public GameObject buildButtonDisabled;
    public GameObject buildEffect;

    int currentLevel;
    

    // Start is called before the first frame update
    void Start()
    {
        currentLevel = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentLevel >= structureLevels.Count)
        {
            buildButtonEnabled.SetActive(false);
            
        }
    }

    public void BuildStructure()
    {
        var Effect = Instantiate(buildEffect, transform.position + new Vector3(0,0,-2),  Quaternion.identity);

        if (currentLevel < structureLevels.Count)
        {
            buildButtonEnabled.SetActive(true);
           
           

            if (currentLevel > 0)
            {
                Animator anim = structureLevels[currentLevel-1].GetComponent<Animator>();
                structureLevels[currentLevel-1].SetActive(false);


            }
            
            structureLevels[currentLevel].SetActive(true);

            currentLevel++;
        }
        else
        {
            buildButtonEnabled.SetActive(false);
           
           
        }
    }

}
