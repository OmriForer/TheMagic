using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPoint : MonoBehaviour
{
    [SerializeField] float randomXZ;
    [SerializeField] float maxY;
    private void OnTriggerEnter(Collider other)
    {
   
        transform.localPosition = GetRandomPos();
    }
    Vector3 GetRandomPos()
    {
        var x = Random.Range(-randomXZ, randomXZ);
        var z = Random.Range(-randomXZ, randomXZ);
        var y = Random.Range(0, maxY);
        var newposition = new Vector3(x, y, z);
        return newposition;
    }
}
