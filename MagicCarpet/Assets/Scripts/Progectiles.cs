using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Progectiles : MonoBehaviour
{
    [SerializeField] float secondsToDestroy = 3;
    [SerializeField] int dmg;
    [Header("Speed Controll")]
    [SerializeField] float speed;
    [SerializeField] float rotateSpeed;
    [SerializeField] bool randomSpeed;
    [SerializeField] float maxRandomSpeed;
    [SerializeField] float minRandomSpeed;
    [Header("Lock Targget")]
    [SerializeField] bool lockOnTarget;
    [SerializeField] float degreesPerSecond;
    [SerializeField] float lockAcceleration;
    [Header("Lock To Hand")]
    [SerializeField] bool lockHand;
    Vector3 lockPose;
    Player movement;
    [SerializeField] GameObject[] trail;
    [SerializeField] GameObject impactEffect;
    private void Awake()
    {
        if (randomSpeed)
        {
            speed = Random.Range(minRandomSpeed, maxRandomSpeed);
        }
        movement = GameObject.Find("Player").GetComponent<Player>();
        lockPose = movement.hitPos;
    }
    void Update()
    {

        transform.Rotate(new Vector3(0, 0, rotateSpeed));
        transform.position += transform.forward * speed * Time.deltaTime;

        DestroyOverTime();

        LockToHand();

        LockOnTarget();
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Ground"))
        {
            ProgectilesHit();
        }

        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("EnemyIgnor"))
        {
            other.GetComponentInParent<Enemy>().healthSystem.Damage(dmg);

            ProgectilesHit();
        }

    }
    void LockToHand()
    {
        if (lockHand)
        {
            transform.position = movement.shootPoint.position;
            transform.eulerAngles = movement.shootPoint.eulerAngles;
        }
    }

    void LockOnTarget()
    {
        if (!lockOnTarget)
            return;
        degreesPerSecond = Mathf.Lerp(degreesPerSecond, 0, lockAcceleration * Time.deltaTime);
        {
            Vector3 dirFromeMeToTarget = lockPose - transform.position;
            Quaternion LookRotation = Quaternion.LookRotation(dirFromeMeToTarget);

            transform.rotation = Quaternion.Lerp(transform.rotation, LookRotation, degreesPerSecond * Time.deltaTime);
        }
    }

    void ProgectilesHit()
    {
        if (impactEffect == null) return;

        TrailDestroy();
        Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    void TrailDestroy()
    {
        if (trail.Length > 0)
        {
            for (int i = 0; i < trail.Length; i++)
            {
                trail[i].transform.parent = null;
                var ps = trail[i].GetComponent<ParticleSystem>();

                ps.Stop();

            }
        }
    }
    
    void DestroyOverTime()
    {
        secondsToDestroy -= Time.deltaTime;
        if (secondsToDestroy <= 0)
        {
            TrailDestroy();
            Destroy(gameObject);
        }
    }
}
