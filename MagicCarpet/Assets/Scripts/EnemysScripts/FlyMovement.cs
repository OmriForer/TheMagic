using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyMovement : Movement
{
    [HideInInspector]
    public GameObject player;
    public float degreesPerSecond;
    [SerializeField] float accelerationMove;
    [SerializeField] float followPlayerRadius;
    [SerializeField] float patrolSpeed;
    [SerializeField] float patrolDegreesPerSecond;
    [SerializeField] Transform partrolPoint;
    float speedbase;
    bool isFollowPlayer;
    Vector3 dirFromeMeToTarget;
    Enemy enemyScript;
    Rigidbody rb;
    [SerializeField] float followIfGotHitTime;
    float followTimer;

    private void Start()
    {
        player = GameObject.Find("Player");
        enemyScript = gameObject.GetComponent<Enemy>();
        rb = GetComponent<Rigidbody>();
    }
    public override void ApplyMovement()
    {
        isFollowPlayer = Physics.CheckSphere(transform.position, followPlayerRadius, enemyScript.playerLayer);


        if (isFollowPlayer || followTimer > Time.time)
        {
            CloseToPlayer();
            FollowPlayer(degreesPerSecond, player.transform.position + new Vector3(0, 1, 0) - transform.position);
        }
        else
        {
            NotFollwPlayer();
        }

        GoForward(speedbase);
        rb.velocity = new Vector3(0, 0, 0);

    }

    public void GoForward(float speed)
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void NotFollwPlayer()
    {
        speedbase = Mathf.Lerp(speedbase, patrolSpeed, accelerationMove * Time.deltaTime);
        dirFromeMeToTarget = partrolPoint.position - transform.position;
        Quaternion LookRotation = Quaternion.LookRotation(dirFromeMeToTarget);
        transform.rotation = Quaternion.Lerp(transform.rotation, LookRotation, patrolDegreesPerSecond * Time.deltaTime);
    }

    public void FollowPlayer(float degreesPerSecond ,Vector3 lookatPos)
    {
        dirFromeMeToTarget = lookatPos;
        Quaternion LookRotation = Quaternion.LookRotation(dirFromeMeToTarget);
        transform.rotation = Quaternion.Lerp(transform.rotation, LookRotation, degreesPerSecond * Time.deltaTime);
    }

    private void CloseToPlayer()
    {
        if (enemyScript.closeToPlayer)
        {
            speedbase = Mathf.Lerp(speedbase, 0, accelerationMove * Time.deltaTime);
        }
        else
        {
            speedbase = Mathf.Lerp(speedbase, speed, accelerationMove * Time.deltaTime);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Magic"))
        {
            followTimer =  Time.time + followIfGotHitTime;
        }
    }

}

