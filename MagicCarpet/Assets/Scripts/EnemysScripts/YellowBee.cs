using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowBee : Enemy
{
    [SerializeField] float timeBetweenAttack;
    float FireRate;
    private void Start()
    {
        healthSystem = new HealthSystem(EnemyHealth);
        healthBar.Setup(healthSystem);
    }
    private void Update()
    {
        SimpleEnemy();
        if (die) return;

        if (closeToPlayer && FireRate < Time.time)
        {
            attackScript.ApplyAttack();
            FireRate = Time.time + timeBetweenAttack;
        }
        else
        {
            movementScript.ApplyMovement();
        }

    }
}
