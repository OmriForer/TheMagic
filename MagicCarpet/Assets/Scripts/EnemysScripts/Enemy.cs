using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Movement movementScript;
    public Attack attackScript;
    public HealthBar healthBar;
    public Transform helthBarLook;
    public HealthSystem healthSystem;
    public LayerMask playerLayer;
    public int EnemyHealth;
    public float radiusToAttack;
    [HideInInspector]
    public bool closeToPlayer;
    [SerializeField] Animator animator;
    public bool die;
    public virtual void SimpleEnemy()
    {
        closeToPlayer = Physics.CheckSphere(transform.position, radiusToAttack, playerLayer);

        if (healthSystem.health <= 0)
        {
            if(!die)
            {
                die = true;
                GetComponent<Rigidbody>().useGravity = true;
            }
            animator.SetTrigger("Die");

        }
        helthBarLook.LookAt(Camera.main.transform);
    }
}
