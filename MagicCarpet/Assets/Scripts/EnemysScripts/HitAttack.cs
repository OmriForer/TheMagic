using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitAttack : Attack
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var player = collision.gameObject.GetComponent<Player>();
            player.healthSystem.Damage(dmg);

        }
    }
}
