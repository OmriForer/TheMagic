using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttack : Attack
{
    [SerializeField] float fireRate = 0.1f;
    [SerializeField] float delayAttack;
    [SerializeField] float delayStopAttack;
    [SerializeField] Animator enemyAnimator;
    [SerializeField] GameObject progectiles;
    [SerializeField] Transform shootPoint;
    [SerializeField] bool shootPointParent;
    public bool startAttack;
    float fireRateTimer;

    private void Update()
    {
        if (startAttack && fireRateTimer < Time.time)
            Shoot();
    }
    public override void ApplyAttack()
    {
        StartCoroutine(AttackAnimation());
    }
    IEnumerator AttackAnimation()
    {
        enemyAnimator.SetTrigger("Attack");
        yield return new WaitForSeconds(delayAttack);
        startAttack = true;

        yield return new WaitForSeconds(delayStopAttack);
        startAttack = false;
    }
    void Shoot()
    {
        if (shootPointParent)
        {
            Instantiate(progectiles, shootPoint.position, shootPoint.rotation, shootPoint);
        }
        else
        {
            Instantiate(progectiles, shootPoint.position, shootPoint.rotation);
        }
        fireRateTimer = Time.time + fireRate;
    }
}
