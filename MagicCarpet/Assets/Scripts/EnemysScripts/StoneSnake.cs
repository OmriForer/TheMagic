using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneSnake : Enemy
{
    [SerializeField] GameObject partical;
    [SerializeField] GameObject headpartical;
    [SerializeField] GameObject[] stones;
    private void Start()
    {
        healthSystem = new HealthSystem(EnemyHealth);
        healthBar.Setup(healthSystem);
    }
    private void Update()
    {
        closeToPlayer = Physics.CheckSphere(transform.position, radiusToAttack, playerLayer);

        SimpleEnemy();
        if (die)
        {
            for (int i = 0; i < stones.Length; i++)
            {
                if(i == 0)
                    Instantiate(headpartical, stones[i].transform.position, Quaternion.identity);

                var prefab = Instantiate(partical, stones[i].transform.position, Quaternion.identity);
                var scaleDrop = 2f - i * 0.13f;
                prefab.transform.localScale = new Vector3(scaleDrop, scaleDrop, scaleDrop);
            }
            Destroy(gameObject);
        }
        movementScript.ApplyMovement();
    }
}
