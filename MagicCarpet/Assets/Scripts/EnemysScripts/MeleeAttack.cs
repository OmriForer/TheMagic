using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : Attack
{
    [SerializeField] float delayAttack;
    [SerializeField] float distanceAttack;
    [SerializeField] Animator enemyAnimator;
    Enemy enemyScript;

    private void Start()
    {
        enemyScript = gameObject.GetComponent<Enemy>();
    }
    public override void ApplyAttack()
    {
        StartCoroutine(AttackDelay());
    }
    IEnumerator AttackDelay()
    {
        enemyAnimator.SetTrigger("Attack");
        yield return new WaitForSeconds(delayAttack);
        RaycastHit hit;
        
        if (Physics.Raycast(transform.position, transform.forward, out hit, distanceAttack, enemyScript.playerLayer))
        {
            hit.transform.gameObject.GetComponent<Player>().healthSystem.Damage(dmg);
        }
    }
}
