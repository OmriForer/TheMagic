using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redDragon : Enemy
{
    [SerializeField] FlyMovement FlyMovementScript;
    [SerializeField] RangeAttack rangeAttackScript;
    [SerializeField] float timeBetweenShootingFire;
    [SerializeField] float lookAtPlayerWhaenShootFire;
    [SerializeField] float lookAtPlayerAfterShootFire;
    [SerializeField] float speedAfterAttack;
    Vector3 playerLookAt;
    Rigidbody rb;

    float TimerToShootFire;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        healthSystem = new HealthSystem(EnemyHealth);
        healthBar.Setup(healthSystem);

    }
    private void Update()
    {
        SimpleEnemy();
        if (rangeAttackScript.startAttack && die)
            rangeAttackScript.startAttack = false;

        if (die) return;

        rb.velocity = new Vector3(0, 0, 0);

        if (closeToPlayer)
        {
            var playerVelocity = FlyMovementScript.player.GetComponent<CharacterController>().velocity;
            var distance = Vector3.Distance(transform.position, FlyMovementScript.player.transform.position);
            if(distance > 30)
            {
                playerLookAt = FlyMovementScript.player.transform.position + new Vector3(0, 1, 0) + playerVelocity - transform.position;
            }
            else
            {
                playerLookAt = FlyMovementScript.player.transform.position - transform.position;
            }

            if (TimerToShootFire <= 0)
            {
                attackScript.ApplyAttack();
                TimerToShootFire = timeBetweenShootingFire;
            }
            if (rangeAttackScript.startAttack)
            {
                FlyMovementScript.FollowPlayer(lookAtPlayerWhaenShootFire, playerLookAt);
            }
            else
            {

                FlyMovementScript.FollowPlayer(lookAtPlayerAfterShootFire, playerLookAt);
                FlyMovementScript.GoForward(speedAfterAttack);
            }       
        }
        else
        {
            movementScript.ApplyMovement();
        }


        if(TimerToShootFire > 0)
        {
            TimerToShootFire -= Time.deltaTime;
        }
        
        
    }
}
