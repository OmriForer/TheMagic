using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiggingMovement : Movement
{
    [SerializeField] float accelerationUp;
    [SerializeField] float accelerationDown;
    [SerializeField] float rotateAccelerationMax;
    [SerializeField] float degreesPerSecond;
    [SerializeField] float GoDownSpeed = 500f;
    [SerializeField] float rotateSpeed;
    [SerializeField] LayerMask ground;
    [SerializeField] Transform tail;
    [SerializeField] Transform head;
    public Transform partrolPoint;
    bool up;
    float rotateAcceleration;
    float rotateAcceleration2;
    Vector3 dirFromeMeToTarget;
    Enemy enemy;
    Rigidbody rb;
    GameObject player;
    float followTimer;
    [SerializeField] float followIfGotHitTime;

    private void Start()
    {
        enemy = GetComponent<Enemy>();
        rb = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
    }
    public override void ApplyMovement()
    {
        head.Rotate(rotateSpeed * Time.deltaTime, 0, 0);
        transform.position += transform.forward * speed * Time.deltaTime;

        if (enemy.closeToPlayer || followTimer > Time.time)
        {
            Attack();
        }
        else
        {
            Patrol();
        }
        rb.velocity = new Vector3(0, 0, 0);


        bool grounded = (Physics.Raycast(tail.position, Vector3.down, 1000f, ground));
        if (grounded)
        {
            OnGround();
        }
        else
        {
            UnderGround();
        }
        rotateAcceleration = Mathf.Lerp(rotateAcceleration, rotateAccelerationMax, accelerationUp * Time.deltaTime);
        rotateAcceleration2 = Mathf.Lerp(rotateAcceleration, rotateAccelerationMax, accelerationDown * Time.deltaTime);
    }
    void Attack()
    {
        dirFromeMeToTarget = player.transform.position + new Vector3(0, 4, 0) + player.GetComponent<Player>().controller.velocity - transform.position;
    }
    void Patrol()
    {
        dirFromeMeToTarget = partrolPoint.position - transform.position;
    }
    void OnGround()
    {
        if (up)
        {
            up = false;
            rotateAcceleration = 0;
            rotateAcceleration2 = 0;
        }
        transform.localEulerAngles += Vector3.right * GoDownSpeed * rotateAcceleration * Time.deltaTime;
    }
    void UnderGround()
    {
        Quaternion LookRotation = Quaternion.LookRotation(dirFromeMeToTarget);
        if (!up)
        {
            up = true;
            rotateAcceleration = 0;
            rotateAcceleration2 = 0;
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, LookRotation, degreesPerSecond * rotateAcceleration2 * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Magic"))
        {
            followTimer = Time.time + followIfGotHitTime;
        }
    }

}
