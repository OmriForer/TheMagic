using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Player : MonoBehaviour
{
    [SerializeField] int maxStamina;
    [SerializeField] float staminaPerSec;
    [HideInInspector]
    public float moveSpeed;
    float upDownSpeed;
    [Header("Movement Controll")]
    public float flySpeed;
    public float ySpeed;
    public float acceleration;
    [SerializeField] float stopAcceleration;
    [SerializeField] float maxHight;
    [SerializeField] float minHight;

    [Header("Carpet Graphics")]
    [SerializeField] float turnAcceleration;
    [SerializeField] float turnAmount;
    public float turnSmooth;

    [Header("References")]
    [SerializeField] Transform playerCamera;
    [SerializeField] Transform carpit;
    public Animator mageAnim;
    public Animator carpetAnimator;
    [SerializeField] CinemachineFreeLook cameraBrain;
    public HealthSystem healthSystem;
    public StaminaSystem staminaSystem;
    public HealthBar healthBar;
    public StaminaBar staminaBar;
    public Transform shootPoint;
    public CharacterController controller;
    public LayerMask ground;
    [HideInInspector]
    public Vector3 hitPos;
    Vector3 direction;
    Vector3 velocity;
    float smoothVelocity;
    float x;
    float z;
    float y;
    float angle;
    float turnRight;
    float turnForward;
    private void Start()
    {
        healthSystem = new HealthSystem(100);
        staminaSystem = new StaminaSystem(maxStamina);
        healthBar.Setup(healthSystem);
        staminaBar.Setup(staminaSystem);
        controller = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        CarpetAnim();
        Aim();

        MovementInput();
        LimitedMovement();
        Movement();
        ControllSpeed();

        GraphicsMovement();

        StaminaRegen();
    }
    void MovementInput()
    {
        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");
        y = Input.GetAxisRaw("UpDown");
    }
    void Movement()
    {
        if (y != 0)
            velocity = transform.up * y;

        if (x != 0 || z != 0)
            direction = transform.right * x + transform.forward * z;


        float targetAngle = playerCamera.eulerAngles.y;
        angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref smoothVelocity, turnSmooth);

        transform.rotation = Quaternion.Euler(0, angle, 0);

        controller.Move((direction.normalized * moveSpeed + velocity * upDownSpeed) * Time.deltaTime);
    }
    void ControllSpeed()
    {
        if (x != 0 || z != 0)
        {
            moveSpeed = Mathf.Lerp(moveSpeed, flySpeed, acceleration * Time.deltaTime);
        }
        else
        {
            moveSpeed = Mathf.Lerp(moveSpeed, 0, stopAcceleration * Time.deltaTime);
        }

        if (y != 0)
        {
            upDownSpeed = Mathf.Lerp(upDownSpeed, ySpeed, acceleration * Time.deltaTime);
        }
        else
        {
            upDownSpeed = Mathf.Lerp(upDownSpeed, 0, stopAcceleration * Time.deltaTime);
        }
    }
    void GraphicsMovement()
    {
        turnRight = Mathf.Lerp(turnRight, x * turnAmount, turnAcceleration * Time.deltaTime);
        turnForward = Mathf.Lerp(turnForward, z * turnAmount, turnAcceleration * Time.deltaTime);
        carpit.rotation = Quaternion.Euler(turnForward, angle, -turnRight);

        mageAnim.SetFloat("Blend", cameraBrain.m_YAxis.Value);
    }
    void Aim()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            shootPoint.LookAt(hit.point);
            hitPos = hit.point;
        }
        else
        {
            shootPoint.LookAt(ray.GetPoint(50));
            hitPos = ray.GetPoint(50);
        }
    }
    void CarpetAnim()
    {
        carpetAnimator.SetFloat("Y", -turnForward);
        carpetAnimator.SetFloat("X", turnRight);
    }
    void StaminaRegen()
    {
        if (maxStamina > staminaSystem.GetStamina())
            staminaSystem.StaminaRegenerate(staminaPerSec * Time.deltaTime);
    }
    void LimitedMovement()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -transform.up, out hit, minHight, ground))
        {
            y = 1;
        }
        if (!Physics.Raycast(transform.position, -transform.up, out hit, maxHight, ground))
        {
            y = -1;
        }
        if (Physics.Raycast(transform.position, -transform.up, out hit, minHight + 2, ground) && y == -1)
        {
            y = 0;
        }
        if (!Physics.Raycast(transform.position, -transform.up, out hit, maxHight - 2, ground) && y == 1)
        {
            y = 0;
        }
    }

}

