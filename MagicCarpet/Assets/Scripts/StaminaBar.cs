using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    [SerializeField] Image staminaBar;
    StaminaSystem staminaSystem;
    public void Setup(StaminaSystem staminaSystem)
    {
        this.staminaSystem = staminaSystem;

        staminaSystem.OnStaminaChanged += StaminaSystem_OnStaminaChanged;
    }
    void StaminaSystem_OnStaminaChanged(object sender, System.EventArgs e)
    {
        staminaBar.fillAmount = staminaSystem.GetStaminaPrecent();
    }
}
