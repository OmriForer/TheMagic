using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityHolder : MonoBehaviour
{
    public Ability[] ability;
    public float[] cooldownTime;
    public float[] activeTime;
    public bool[] activeAbility;
    int currentAbilityIndex;
    Player player;
    float timer;
   


    enum AbilityState
    {
        ready,
        active,
        cooldown
    }
    AbilityState[] state;

    public KeyCode key;

    private void Awake()
    {
        player = GetComponent<Player>();
        state = new AbilityState[ability.Length];
    }
    void Update()
    {
        ChooseAbility();
        AbilitysStateLogic();
        UseAbility();
    }
    void ChooseAbility()
    {
        if (activeTime[currentAbilityIndex] <= 0.1f || !activeAbility[currentAbilityIndex])
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                currentAbilityIndex = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                currentAbilityIndex = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                currentAbilityIndex = 2;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                currentAbilityIndex = 3;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                currentAbilityIndex = 4;
            }
        }
    }
    void UseAbility()
    {

        if (Input.GetKey(key) && !GameManager.activeAbility && state[currentAbilityIndex] == AbilityState.ready)
        {
            if (activeAbility[currentAbilityIndex])
                GameManager.activeAbility = true;

            if (ability[currentAbilityIndex].manaCost > player.staminaSystem.GetStamina())
                return;

            ability[currentAbilityIndex].Activate(gameObject);
            state[currentAbilityIndex] = AbilityState.active;
            activeTime[currentAbilityIndex] = ability[currentAbilityIndex].activeTime;
        }
    }
    void AbilitysStateLogic()
    {
        for (int i = 0; i < ability.Length; i++)
        {
            switch (state[i])
            {
                case AbilityState.ready:
                    break;

                case AbilityState.active:
                    if (activeTime[i] > 0)
                    {
                        ability[i].WhileActivate(gameObject);
                        NoActiveTime(i);
                    }
                    else
                    {
                        GameManager.activeAbility = false;
                        ability[i].BeginCooldown(gameObject);
                        state[i] = AbilityState.cooldown;
                        cooldownTime[i] = ability[i].cooldownTime;
                    }
                    break;
                case AbilityState.cooldown:
                    if (cooldownTime[i] > 0)
                    {
                        cooldownTime[i] -= Time.deltaTime;
                    }
                    else
                    {
                        state[i] = AbilityState.ready;
                    }
                    break;

            }
        }
    }
    void NoActiveTime(int i)
    {
        if(ability[i].isNoActiveTime)
        {
            if (timer < ability[i].fastClickNoActiveTime)
            {
                timer += Time.deltaTime;
                return;
            }

                if (!Input.GetKey(key) || ability[i].manaCost > player.staminaSystem.GetStamina())
                {
                    GameManager.activeAbility = false;
                    ability[i].BeginCooldown(gameObject);
                    state[i] = AbilityState.cooldown;
                    cooldownTime[i] = ability[i].cooldownTime;
                    timer = 0;
                }
            
        }
        else
        {
            activeTime[i] -= Time.deltaTime;
        }
    }
}
