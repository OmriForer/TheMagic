using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] bool InstantiateEnemy;
    [SerializeField] LayerMask player;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] float radiusToSpawn;
    public List<GameObject> enemysList = new List<GameObject>();
    public List<GameObject> patrolsPoint = new List<GameObject>();
    [SerializeField] float timeToCheck;
    float timer;



    private void Update()
    {
        if (timer > Time.time) return;

        timer = timeToCheck + Time.time;

        if (Physics.CheckSphere(transform.position, radiusToSpawn, player))
            return;

        if (!InstantiateEnemy)
        {
            for (int i = 0; i < enemysList.Count; i++)
            {
                if (enemysList[i].GetComponent<Enemy>().die)
                {
                    enemysList[i].GetComponent<Enemy>().die = false;
                    var healthSystem = enemysList[i].GetComponent<Enemy>().healthSystem;
                    var maxHealth = healthSystem.GetMaxHealth();

                    healthSystem.Heal(maxHealth);

                    enemysList[i].transform.position = transform.position;
                    enemysList[i].SetActive(false);
                    enemysList[i].SetActive(true);
                }
            }
        }
        else
        {
            for (int i = 0; i < enemysList.Count; i++)
            {
                if (enemysList[i] == null)
                {
                    var enemy = Instantiate(enemyPrefab ,transform.position, transform.rotation);
                    enemy.GetComponent<DiggingMovement>().partrolPoint = patrolsPoint[i].transform;
                    enemysList[i] = enemy;


                }
            }
        }
    }
}
