using System;

public class StaminaSystem
{
    public event EventHandler OnStaminaChanged;

    public float Stamina;
    private int MaxStamina;
    public StaminaSystem(int maxStamina)
    {
        this.MaxStamina = maxStamina;
        Stamina = maxStamina;
    }
    public float GetStamina()
    {
        return Stamina;
    }
    public float GetStaminaPrecent()
    {
        return (float)Stamina / MaxStamina;
    }
    public void UseStamina(float staminaCost)
    {
        Stamina -= staminaCost;
        if (Stamina < 0) Stamina = 0;
        if (OnStaminaChanged != null) OnStaminaChanged(this, EventArgs.Empty);
    }
    public void StaminaRegenerate(float staminaPerSec)
    {
        Stamina += staminaPerSec;
        if (Stamina > MaxStamina) Stamina = MaxStamina;
        if (OnStaminaChanged != null) OnStaminaChanged(this, EventArgs.Empty);
    }

}
