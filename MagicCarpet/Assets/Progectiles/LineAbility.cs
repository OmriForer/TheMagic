using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CreateAssetMenu]
public class LineAbility : Ability
{
    [SerializeField] float offsetTime;
    [SerializeField] float offsetTimeEnd;
    [SerializeField] float fireRate;

    [Header("References")]
    [SerializeField] GameObject magicPrefab;
    [SerializeField] string boolString;
    Transform shootPoint;
    float timer;
    float time;
    Player player;
    bool firstShot;

    public override void Activate(GameObject parent)
    {
        player = parent.GetComponent<Player>();
        shootPoint = player.shootPoint;
        player.mageAnim.SetBool(boolString, true);
        timer = 0;
        time = 0;
    }
    public override void WhileActivate(GameObject parent)
    {
        player.staminaSystem.UseStamina(manaCost * Time.deltaTime);

        time += Time.deltaTime;

        if (timer < Time.time && offsetTime < time)
        {
            Shoot();
            timer = Time.time + fireRate;
        }
    }

    private void Shoot()
    {
        var PrefabAbillity = Instantiate(magicPrefab, shootPoint.position, shootPoint.rotation);
    }
    public override void BeginCooldown(GameObject parent)
    {
        player.mageAnim.SetBool(boolString, false);
    }
}
