using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CreateAssetMenu]
public class ShootAbility : Ability
{
    [SerializeField] bool shootOnce;
    [SerializeField] float offsetTime;
    [SerializeField] float offsetTimeEnd;

    [Header("Multy Shots")]
    [SerializeField] int shotsNumber = 1;

    [Header("Spray Shots")]
    [SerializeField] float timeBetweenShots;
    [SerializeField] bool spreadShots;
    [SerializeField] bool onlyOnEdges;
    [SerializeField] float spreadAmount;

    [Header("Arc Shots")]
    [SerializeField] bool arc;
    [SerializeField] float arcRange;
    [SerializeField] float MaxArcTime;
    [SerializeField] float minArcTime;

    [Header("References")]
    [SerializeField] GameObject magicPrefab;
    [SerializeField] string triggerString;
    Transform shootPoint;
    float timer;
    float time;
    bool cantShoot;
    public override void Activate(GameObject parent)
    {
        Player movement = parent.GetComponent<Player>();
        movement.staminaSystem.UseStamina(manaCost);
        shootPoint = movement.shootPoint;

        movement.mageAnim.SetTrigger(triggerString);
        timer = 0;
        time = 0;
    }
    public override void WhileActivate(GameObject parent)
    {

        time += Time.deltaTime;

        if (cantShoot)
            return;

        if (timer <= 0 && offsetTime < time && offsetTimeEnd > time)
        {
            Shoot();

            if (shootOnce)
                cantShoot = true;
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    private void Shoot()
    {
        for (int i = 0; i < shotsNumber; i++)
        {
            var PrefabAbillity = Instantiate(magicPrefab, shootPoint.position, shootPoint.rotation);
            ArcAbility(PrefabAbillity);

            if (spreadShots)
            {
                SprayShot(PrefabAbillity);
            }
            timer = timeBetweenShots;
        }
    }

    private void SprayShot(GameObject PrefabAbillity)
    {
        float random1 = Random.Range(-1f, 1f);
        float random2 = Random.Range(-1f, 1f);
        if (!onlyOnEdges)
        {
            PrefabAbillity.transform.eulerAngles += new Vector3(random1, random2, 0) * spreadAmount;
        }
        else
        {
            PrefabAbillity.transform.eulerAngles += new Vector3(random1, random2, 0).normalized * spreadAmount;
        }
    }

    private void ArcAbility(GameObject PrefabAbillity)
    {
        if (arc)
            iTween.PunchPosition(PrefabAbillity, new Vector3(Random.Range(-arcRange, arcRange), Random.Range(-arcRange, arcRange), 0), Random.Range(MaxArcTime, minArcTime));
    }

    public override void BeginCooldown(GameObject parent)
    {
        cantShoot = false;
    }
}
