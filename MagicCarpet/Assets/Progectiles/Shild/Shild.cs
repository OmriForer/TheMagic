using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Shild : Ability
{
    [SerializeField] GameObject sandShild;
    [SerializeField] string triggerString;
    Player player;
    GameObject thisShild;
    public override void Activate(GameObject parent)
    {
        player = parent.GetComponent<Player>();
        thisShild = Instantiate(sandShild, parent.transform.position + new Vector3(0,1,0), parent.transform.rotation, parent.transform);

        player.mageAnim.SetBool(triggerString,true);
    }
    public override void WhileActivate(GameObject parent)
    {
        player.staminaSystem.UseStamina(manaCost * Time.deltaTime);
    }
    public override void BeginCooldown(GameObject parent)
    {
        thisShild.GetComponent<Animator>().SetTrigger("Finish");
        Destroy(thisShild,1);
        player.mageAnim.SetBool(triggerString, false);
        base.BeginCooldown(parent);
    }
}
