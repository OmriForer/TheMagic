using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnShild : MonoBehaviour
{
    [SerializeField] Transform secondShild;
    [SerializeField] Vector3 rotate;

    void Update()
    {
        secondShild.transform.Rotate(rotate.x, rotate.y, rotate.z); 
        transform.Rotate(-rotate.x, -rotate.y,-rotate.z); 
    }
}
