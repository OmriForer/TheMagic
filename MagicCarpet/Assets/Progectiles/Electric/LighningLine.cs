using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighningLine : MonoBehaviour
{

    LineRenderer lr;
    public Transform pos1;
    public Transform pos2;
    void Start()
    {
        Destroy(gameObject, 0.2f);
        lr = GetComponent<LineRenderer>();
        lr.positionCount = 2;
    }
    void Update()
    {
        if (pos1 == null || pos2 == null)
        {
            Destroy(gameObject);
        }
    }
}
