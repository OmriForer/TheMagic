using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ElectricCollision : MonoBehaviour
{
    [SerializeField] int dmg;
    [SerializeField] int bouncingDmg;
    [SerializeField] float radius;
    [SerializeField] LayerMask Enemys;
    [SerializeField] GameObject hitEffect;
    [SerializeField] GameObject lineRenderer;



    private void OnTriggerEnter(Collider other)
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.forward, out hit, Mathf.Infinity);

        var pos = hit.point;

         if (other.gameObject.CompareTag("EnemyIgnor"))
        {
            other.GetComponentInParent<Enemy>().healthSystem.Damage(dmg);
            Destroy(gameObject);
            Instantiate(hitEffect, pos, transform.rotation);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            other.GetComponentInParent<Enemy>().healthSystem.Damage(dmg);
            ChainImpact(other.gameObject);

           Instantiate(hitEffect, pos, transform.rotation);
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Ground"))
        {
            Instantiate(hitEffect, pos, transform.rotation);
            Destroy(gameObject);
        }
    }
    private void ChainImpact(GameObject other)
    {
        Collider[] hitColliders = Physics.OverlapSphere(other.transform.position, radius, Enemys);

        foreach (var hitCollider in hitColliders)
        {
            Vector3 diff = hitCollider.transform.position - other.gameObject.transform.position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance > 0.1f)
            {
                hitCollider.gameObject.GetComponentInParent<Enemy>().healthSystem.Damage(bouncingDmg);
                var Lr = Instantiate(lineRenderer);

                LighningLine line = Lr.GetComponent<LighningLine>();
                line.pos1 = other.transform;
                line.pos2 = hitCollider.transform;

                Instantiate(hitEffect, hitCollider.transform.position, hitCollider.transform.rotation, hitCollider.transform);
            }
        }
    }
}
