using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HealAbility : Ability
{
    public int healPerSec;
    public GameObject healEffect;
    float timer;
    public override void Activate(GameObject parent)
    {
        Player movement = parent.GetComponent<Player>();
        movement.mageAnim.SetTrigger("Heal");
        Instantiate(healEffect, parent.transform.position, parent.transform.rotation, parent.transform);
        timer = 0;
    }
    public override void WhileActivate(GameObject parent)
    {
        var healingSystem = parent.GetComponent<Player>().healthSystem;

        var heal = healPerSec * Time.deltaTime;

        if (timer < Time.time)
        {
            healingSystem.Heal(healPerSec);
            timer = 0.5f + Time.time;
        }

    }

}
