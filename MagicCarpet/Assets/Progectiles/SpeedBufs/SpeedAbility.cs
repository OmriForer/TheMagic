using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpeedAbility : Ability
{
    public float additionalSpeed;
    public float additionalAcceleration;
    public override void Activate(GameObject parent)
    {
        Player movement = parent.GetComponent<Player>();
        movement.flySpeed += additionalSpeed;
        movement.acceleration += additionalAcceleration;
    }
    public override void BeginCooldown(GameObject parent)
    {
        Player movement = parent.GetComponent<Player>();
        movement.flySpeed -= additionalSpeed;
        movement.acceleration -= additionalAcceleration;
    }
}
