using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class DashAbility : Ability
{
    public float additionalSpeed;
    public float additionalAcceleration;
    public override void Activate(GameObject parent)
    {
        Animator playerAnim = parent.GetComponent<Animator>();
        Player movement = parent.GetComponent<Player>();
        movement.flySpeed += additionalSpeed;
        movement.ySpeed += additionalSpeed/2;
        movement.acceleration += additionalAcceleration;
        playerAnim.SetTrigger("Dash");
    }
    public override void BeginCooldown(GameObject parent)
    {
        Player movement = parent.GetComponent<Player>();
        movement.flySpeed -= additionalSpeed;
        movement.ySpeed -= additionalSpeed/2;
        movement.acceleration -= additionalAcceleration;
        movement.moveSpeed = movement.flySpeed;
    }
}
