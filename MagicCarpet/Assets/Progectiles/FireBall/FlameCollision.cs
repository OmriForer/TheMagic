using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameCollision : MonoBehaviour
{
    [SerializeField] int dmg;
    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    Vector3 position;



    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        int i = 0;
        while (i < numCollisionEvents)
        {
            position = collisionEvents[i].intersection;
            i++;
        }
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Player>().healthSystem.Damage(dmg);
        }

    }
}
